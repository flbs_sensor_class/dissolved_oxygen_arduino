![SensorSpace Logo](https://sensorspace.tech/images/logo1_vertical.png)

# Flathead Lake Biological Station - Commercial Dissolved Oxygen Sensor Example 

In this example we will be communicating with a commercial dissolved oxygen probe over RS485, using the Modbus RTU protocol. The data is then sent online to The Things Network using the onboard LoRaWAN modem. The other half of this project is titled "real_time_data_plotting_server", where we setup a web server to retrieve the data from The Things Network and plot it in real-time.

## Hardware Needed for This Project

 1. PreSens WR-RS485M Dissolved Oxygen Probe https://www.presens.de/products/detail/oxybaser-wr-rs485-wr-rs485m.html
 2. The Things Network Uno (an Arduino Leonardo with an onboard LoRaWAN modem) https://www.newark.com/the-things-network/ttn-un-915/silicon-manufacturer-microchip/dp/05AC1811?COM=dtss-detail_Wireless_Embest_TTN-UN-868%20CMPNULL
 3. RS485 Communication Shield for Arduino http://www.tinyosshop.com/arduino-rs232-rs485-shield
 4. Micro USB cable https://www.amazon.com/AmazonBasics-Male-Micro-Cable-Black/dp/B0711PVX6Z/ref=sr_1_6?s=electronics&ie=UTF8&qid=1529958460&sr=1-6&keywords=micro+usb+cable

## Changes Made to the Hardware

***This stuff has already been done for you!***

 1. Changed the dissolved oxygen probe's baud rate and parity settings to 4800 baud and 8E1 (8 data bits, even parity, 1 stop bit). The default settings are 19200 baud, 8N2. This was done so that the software serial library would work more reliably (slower baud rate = less stringent timing requirements). The hardware serial port on the Things Network Uno board is being used by the on-board LoRaWAN modem, which is why a software-based serial port is necessary.
 2. Soldered pin D2 to pin D8, because pin D2 (the pin that the RS485 shield uses for receiving data with software-based serial communication) lacks pin change interrupts on the Things Network Uno Board, but pin D8 supports them. As a result, we will use pin D8 as the pin from which we receive serial data.
 3. On the RS485 shield, make sure to desolder the "Auto" solder bridge. Then bridge the "Manual" mode connection with some solder. We will set the RS485 data direction manually, in the code (much more reliable).

## Hardware Setup

The manuals for the dissolved oxygen probe can be found in the "Documentation" folder in this project. In one of them you will find the pinout, which tells you what each wire is. Here is that table:

![DO probe pinout](https://s22.postimg.cc/5y533q8xd/image.png)

Now use this table to connect the sensor to the RS485 shield, copying the picture shown below.

 - Brown -> +5V
 - Green -> A
 - Yellow -> B
 - White -> Ground
 - Transparent -> Ground (optional, not connected in the picture)

![Board with sensor probe attached](https://s22.postimg.cc/m7v96gr9d/image.png)

There are two small switches on the RS485 shield. Make sure they are set to "RS485" and "Softserial". Now take the micro USB cable and plug it into The Things Network Uno (the board on the bottom). Plug the other end of the USB cable into your computer.

## Software Setup

In order to download this project, you will need Git. In order to compile the code and load the code onto The Things Uno board, you will need the Arduino IDE (IDE = Integrated Development Environment).

 1. Install Git https://git-scm.com/
 2. Install the Arduino IDE https://www.arduino.cc/en/Main/Software
 
 Once these are installed, open a command line and type the following to verify that Git was installed:
 
   ```console
git --version
```
You should see a response similar to: 

> git version 2.15.1.windows.2

Assuming this worked, now it's time to download this project. In the command line, type:

    cd ~/Desktop

Then type:

    git clone https://gitlab.com/flbs_sensor_class/dissolved_oxygen_arduino.git

You should see some stuff going on in the command line. Git is downloading all of the files from this project onto your desktop, into a folder named "dissolved_oxygen_arduino". After Git is finished, open this folder and select the folders "ModbusMaster", "SoftwareSerialParity", and "TheThingsNetwork". Copy these folders and paste them into your Arduino IDE library folder (for me it is "C:\Users\Zane\Documents\Arduino\libraries").

After these three libraries have been copied over, open the Arduino IDE software (the program you downloaded earlier, along with Git). Just to make sure that the program sees the three new libraries, go to **Sketch->Include Library->Manage Libraries**. The program will now find the three new libraries (if it hadn't already).

## Getting the LoRaWAN Modem ID

Each LoRaWAN modem (including the one on our Things Uno board) has a unique ID associated with it. We need to use this ID in order to register the device on our LoRaWAN network. Let's get that ID!

![LoRaWAN modem](https://s8.postimg.cc/dhnvcwutx/image.png)

Open up the Arduino IDE and go to **File->Examples->TheThingsNetwork->DeviceInfo**. Now that this bit of example code is open, we need to load it onto The Things Uno board. Select **Tools->Board->Arduino Leonardo**. Then select the port that the board is connected to **Tools->Port->COM9 (Arduino Leonardo)**. The COM port number will likely be different for your computer. It should look something like this:

![DeviceInfo](https://s8.postimg.cc/faqs14sud/image.png)

You'll also need to change the code on line 7 so that it looks like this:

![Code change](https://s8.postimg.cc/uw83l64th/image.png)

Here you are telling the LoRaWAN modem to operate at 915MHz, the frequency band allocated for LoRaWAN in the United States (in Europe they use 868MHz).

Now the code is ready to go. Click the **Upload** button in the top left (the arrow pointing to the right) to compile the code and load it onto the Arduino board. Go to **Tools->Serial Monitor** and you will see the output from the board. It should look something like this:

![enter image description here](https://s8.postimg.cc/u996vopxx/image.png)

Copy the DevEUI and save it in a text file on your computer for later use. We will need this ID in the next step!

## Setting up the Device on The Things Network

Now it's time to make an account on The Things Network. https://account.thethingsnetwork.org/register

Once you are finished making an account, go to **Console->Applications** (or click this link https://console.thethingsnetwork.org/applications). Click on **Add Application**, to create a new app. For the application ID, I'm calling mine "flbs_test_application". Name yours whatever you want. At the bottom, make sure **ttn-handler-us-west** is selected! Then click the "Add Application" button at the bottom.

You've created an app and now it's time to add your device to that app. Click on **register device** and then enter any name in the **Device ID** field. I'm calling mine "flbs_dissolved_oxygen_sensor". Now comes the important part! In the **Device EUI** field, paste in the ID (DevEUI) that you saved earlier from the Things Uno Board. Now click the button at the bottom to register the device. 

Click on your newly registered device and find these two fields. You can click on the clipboard icon on the right side to copy each field. You will need to paste these two fields into the dissolved oxygen sensor code example.

![enter image description here](https://s8.postimg.cc/uecy544w5/image.png)

Open up the dissolved oxygen sensor code, titled "dissolved_oxygen_arduino.ino". It's located in the project folder on your desktop. You need to paste your App EUI and App Key from the above fields into these two strings in the code:

![enter image description here](https://s8.postimg.cc/k4aj60zmt/image.png)

## Uploading the Code to the Board

Once you've finished adding your own App EUI and App Key to the code, click the **Upload** button to load the code onto the board. Open up the **Serial Monitor** once again to view the output from The Things Uno board.

At the start of the output, you will see that the board attempts to join the LoRaWAN network (by issuing a **Join Request** packet). If the AppEUI, AppKey, and DevEUI match up with what you entered on The Things Network website, the gateway will send out a **Join Accept** back to the board, and the code will proceed to the next step. The code then tells the board to get a reading from the dissolved oxygen sensor every 5 seconds, and sends that measurement wirelessly over LoRaWAN to The Things Network.

Assuming everything went well, you should see something like this:

![enter image description here](https://s8.postimg.cc/qyh77xslh/image.png)

On the right is the serial output from The Things Uno board. At the start it initializes the LoRaWAN module for use with The Things Network, reads off the dissolved oxygen sensor's firmware version, and it then prints the sampling interval and measurement unit. Every 5 seconds afterwards, it prints the dissolved oxygen reading and displays whether the wireless transmission of that measurement was successful. In this screenshot, only two dissolved oxygen measurements had been sent.

On the left is a view of the device's data, received by The Things Network. The first packet received (the yellow lightning bolt) was a **Join Request** and the following two packets are **Uplinks** (sending data up to the server). The payload is the data we sent. You can see that it's 4 bytes long, because we sent the dissolved oxygen measurement as a floating point value (which is 4 bytes on most embedded systems). 

You might be thinking, how is that series of numbers and letters in my payload my dissolved oxygen measurement!? I sent a value of 7.38 mg/L and in the Things Network display it shows it as FF 3F EC 40!? 

## Decoding the Data

The measurement was sent as a series of four bytes, which The Things Network is currently displaying to you in hexadecimal format. You can add a **decoder** on The Things Network to convert the raw bytes back into a floating point value. 

Go to your application and click on the **Payload Formats** button. Then choose a **Custom**  payload format and select the **decoder**.

Copy and paste the following function into the text box:

    function Decoder(bytes, port) {
    
      // Based on https://stackoverflow.com/a/37471538 by Ilya Bursov
      function bytesToFloat(bytes) {
        var bits = bytes[3]<<24 | bytes[2]<<16 | bytes[1]<<8 | bytes[3];
        var sign = (bits>>>31 == 0) ? 1.0 : -1.0;
        var e = bits>>>23 & 0xff;
        var m = (e == 0) ? (bits & 0x7fffff)<<1 : (bits & 0x7fffff) | 0x800000;
        var f = sign * m * Math.pow(2, e - 150);
        return f;
      }  
    
      // Test with 0082d241 for 26.3135 
      return {
        // Take bytes 0 to 4 (not including), and convert to float:
        dissolved_oxygen: bytesToFloat(bytes.slice(0, 4))
      };
    }

Lastly, enter the value **00 82 D2 41** into the **Payload** field and click the **Test** button to see if the function properly converts the bytes into a floating point value. If everything goes well, you should see that the function converted **00 82 D2 41** into the string **"dissolved_oxygen": 26.313600540161133**. 

Scroll down and click on the **save payload functions** button.


![enter image description here](https://s8.postimg.cc/f839qz7n9/image.png)

## We're finished!

Click on the **Data** button to see your sensor data streaming in. You should now see that the decoder function is converting the raw bytes into a meaningful floating point value.

![enter image description here](https://s8.postimg.cc/cj8h0m22d/image.png)