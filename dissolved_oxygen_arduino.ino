/*****************************************************************************
   Includes
 ****************************************************************************/

#include <SoftwareSerialParity.h>
#include <TheThingsNetwork.h>
#include <ModbusMaster.h>

/*****************************************************************************
   Definitions and Global Variables
 ****************************************************************************/

/* Set your AppEUI and AppKey here */
const char *appEui = "70B3D57ED001014F";
const char *appKey = "2A4F2DF863CD195A3269517541EBFABE";

/* Serial debugging */
#define debugSerial Serial
#define DEBUG_SERIAL_BAUD 9600

/* Modbus stuff */
#define MODBUS_RX_PIN 8
#define MODBUS_TX_PIN 3
#define MODBUS_DIR_PIN  4
#define MODBUS_SERIAL_BAUD 4800
#define MODBUS_SERIAL_PARITY EVEN
SoftwareSerialParity modbusSerial(MODBUS_RX_PIN, MODBUS_TX_PIN);
ModbusMaster node;

/* LoRa stuff */
#define loraSerial Serial1
#define freqPlan TTN_FP_US915
#define LORA_SERIAL_BAUD 57600
TheThingsNetwork ttn(loraSerial, debugSerial, freqPlan);

/*****************************************************************************
   Functions
 ****************************************************************************/

void modbusPreTransmission()
{
  delay(500);
  digitalWrite(MODBUS_DIR_PIN, HIGH);
}

void modbusPostTransmission()
{
  digitalWrite(MODBUS_DIR_PIN, LOW);
  delay(500);
}

int read_do_sensor_firmware_version(char *firmware_version)
{
  int i = 0, result = 0;
  uint16_t data[8];

  /* Read firmware version from the DO sensor - address (1031) and length (8) are from the datasheet */
  result = node.readHoldingRegisters(1031, 8);

  /* Check if data was read successfully */
  if (result != node.ku8MBSuccess)
  {
    debugSerial.println("ERROR -  Couldn't get DO sensor firmware version");
    return -1;
  }

  /* Get the data */
  for (i = 0; i < 8; i++)
  {
    data[i] = node.getResponseBuffer(i);
  }

  /* Copy it over to a byte array and end it with a null byte */
  memcpy(firmware_version, (uint8_t *)data, 16);
  firmware_version[16] = 0;

  return 0;
}

int set_do_sensor_sampling_interval(int32_t seconds)
{
  int result = 0;

  node.setTransmitBuffer(0, lowWord(seconds));
  node.setTransmitBuffer(1, highWord(seconds));
  result = node.writeMultipleRegisters(3499, 2); /* From the datasheet */

  if (result != node.ku8MBSuccess)
  {
    debugSerial.println("ERROR -  Couldn't set DO sensor sampling interval");
    return -1;
  }

  return 0;
}

int read_do_sensor_oxygen_value(float *reading)
{
  int i = 0, result = 0;
  uint16_t data[2];

  /* From the datasheet */
  result = node.readHoldingRegisters(2091, 2);

  /* Check if data was read successfully */
  if (result != node.ku8MBSuccess)
  {
    debugSerial.println("ERROR -  Couldn't get DO sensor measurement");
    return -1;
  }

  /* Get the data */
  data[0] = node.getResponseBuffer(0);
  data[1] = node.getResponseBuffer(1);
  *reading = *((float *)data);

  return 0;
}

int set_do_sensor_unit(int32_t unit)
{
  int result = 0;

  node.setTransmitBuffer(0, lowWord(unit));
  node.setTransmitBuffer(1, highWord(unit));
  result = node.writeMultipleRegisters(2089, 2); /* From the datasheet */

  if (result != node.ku8MBSuccess)
  {
    debugSerial.println("ERROR -  Couldn't set DO sensor oxygen unit");
    return -1;
  }

  return 0;
}

/*****************************************************************************
   Initialization
 ****************************************************************************/

void setup()
{
  /* Initialize serial ports */
  loraSerial.begin(LORA_SERIAL_BAUD);
  debugSerial.begin(DEBUG_SERIAL_BAUD);
  modbusSerial.begin(MODBUS_SERIAL_BAUD, MODBUS_SERIAL_PARITY);

  /* Initialize modbus stuff, use modbus address of 1 to talk to DO sensor (in the datasheet) */
  pinMode(MODBUS_DIR_PIN, OUTPUT);
  digitalWrite(MODBUS_DIR_PIN, LOW);
  node.begin(1, modbusSerial);
  node.preTransmission(modbusPreTransmission);
  node.postTransmission(modbusPostTransmission);

  /* Try to connect to TTN */
  debugSerial.println("--LORAWAN STATUS--");
  ttn.showStatus();
  debugSerial.println("--LORAWAN JOIN REQUEST--");
  ttn.join(appEui, appKey);

  /* Initialize DO sensor */
  char firmware_version[32];
  if (read_do_sensor_firmware_version(firmware_version) == 0)
  {
    debugSerial.println();
    debugSerial.print("DO Sensor Firmware Version: ");
    debugSerial.println(firmware_version);

    /* Make a measurement every 5 seconds */
    if (set_do_sensor_sampling_interval(5) == 0)
    {
      debugSerial.print("DO Sensor Sampling Interval: ");
      debugSerial.println(5);

      /* Make sure measurements are in mg/L, use value 0x80 (from datasheet) */
      if (set_do_sensor_unit(0x80) == 0)
      {
        debugSerial.println("DO Sensor Measurement Unit = mg/L");
        debugSerial.println();
      }
    }
  }
}

/*****************************************************************************
   Main Loop
 ****************************************************************************/

void loop()
{
  float do_reading;

  if (read_do_sensor_oxygen_value(&do_reading) == 0)
  {
    debugSerial.println("-----------------------------------------------------------------------------");
    debugSerial.print("DO: ");
    debugSerial.print(do_reading);
    debugSerial.println(" mg/L");
    ttn.sendBytes((uint8_t*)&do_reading, 4);
    debugSerial.println("-----------------------------------------------------------------------------");
    debugSerial.println();
    debugSerial.println();
  }

  delay(5000);
}
